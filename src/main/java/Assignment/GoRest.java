package Assignment;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.*;



import org.json.simple.JSONObject;


public class GoRest {
	
	   @Test
	    public void testStatusCode() {
	        baseURI = "https://gorest.co.in/";
	        given().get("public/v2/users").then().statusCode(200);
	}
	    @Test
	    public void firstGetMethod() {
	        Response res = RestAssured.get("https://gorest.co.in/public/v2/users/2799");
	        int statusCode = res.getStatusCode();
	        System.out.println(statusCode);
	        System.out.println(res.getBody().asString());
	    }    @Test
	    private void assertGetMethod() {        
	    	Response res = RestAssured.get("https://gorest.co.in/public/v2/users");
	    }
	        int statusCode = res.getStatusCode();
	        Assert.assertEquals(200, statusCode);    }
	    @Test
	    public void postOperationTest() {
	        JSONObject req = new JSONObject();
	          req.put("name","Aditi");
	          req.put("email", "aditi122@gmail.com");
	          req.put("gender", "female");
	          req.put("status", "Active");
	          System.out.println(req);
	          baseURI = "https://gorest.co.in/public/v2";
	          given().log().all().contentType("application/json").header("authorization","Bearer bc539c3d6c3b49d96e2333a9861fefbd3da867dd9802dbcb053395f2e409f59c").body(req.toJSONString()).when().post("/users").then().statusCode(201);
	         

}
