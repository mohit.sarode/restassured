package apiTesting;

import org.testng.annotations.Test;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;


public class PatchRequest {
  @Test
  public void TestPatchRequest() {
	  
	  JSONObject request =new JSONObject();
      request.put("name", "aditi");
      request.put("job", "tester");
      
      
      given().body(request.toJSONString())
      .patch("https://reqres.in/api/users/2")
      .then()
      .statusCode(200).body("updatedAt", greaterThanOrEqualTo("2023-01-24T09:50:12.293Z"));
	  
      given()
      .body(request.toJSONString())
      .patch("https://reqres.in/api/users/2")
      .then()
      .statusCode(200)
      .body("updatedAt", greaterThanOrEqualTo("2023-01-24T09:50:12.293Z"));     
      
	  
	  
  }
}
