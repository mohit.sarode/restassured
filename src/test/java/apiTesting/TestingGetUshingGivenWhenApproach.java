package apiTesting;

import org.testng.annotations.Test;
import  static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class TestingGetUshingGivenWhenApproach {
	
	@Test
	
	public void testStatusCode() {
		
		given()
		.get("https://reqres.in/api/users?page=2").then().statusCode(200);
		
	}
	@Test
	 public void BAseUri() {
		 baseURI = "https://reqres.in";
		 given()
			.get("/api/users?page=2").then().statusCode(200);
		 
	 }
	 
	@Test
	 public void testparticularValue() {
		 baseURI = "https://reqres.in";
		 given()
			.get("/api/users?page=2").then().body("data[1].email", equalTo("lindsay.ferguson@reqres.in"));
		 
	 }
	 
	  @Test
	  public void printAllValues()
	  {
	      baseURI="https://reqres.in";
	      given().get("/api/users?page=2").then().log().all();
	  }

	  @Test
		 public void testparticularValue1() {
			 baseURI = "https://reqres.in";
			 given()
				.get("/api/users/2").then().body("data.last_name", equalTo("Weaver"));
	  }
}
