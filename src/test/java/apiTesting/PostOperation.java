package apiTesting;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class PostOperation {
  @Test
  public void postOperationTest() {
	  
	  JSONObject request=new JSONObject();
      request.put("name","manzoor");
      request.put("job","tester");
      System.out.println(request);
      
      baseURI="https://reqres.in/api";
         given().body(request.toJSONString()).when().post("/users").then().statusCode(201);
  }
	  
  }

