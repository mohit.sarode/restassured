package apiTesting;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class TestingGET_Request {
  @Test
  public void firstGET_Method() {
	  
	 Response response = RestAssured.get("https://reqres.in/api/users?page=2");
	 
	 int StatusCode = response.getStatusCode();
	 System.out.println(StatusCode);
	 System.out.println(response.getBody().asString());
  }
  
  @Test
  public void assertGetMethod() {
	  Response response = RestAssured.get("https://reqres.in/api/users?page=2");
		 
		 int StatusCode = response.getStatusCode();
		 Assert.assertEquals(200, StatusCode);
	  
	  
  }
 
  
}
