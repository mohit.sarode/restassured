package apiTesting;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.*;

import org.testng.annotations.Test;

public class GetMultiplevalue {
	
	@Test
	 public void singleValue() {
		
		 baseURI = "https://reqres.in";
		 given()
			.get("/api/users?page=2").then().body("data.first_name", hasItem("Lindsay"));
	}
	

	
	@Test
	 public void multiple_value() {
		 baseURI = "https://reqres.in";
		 given()
			.get("/api/users?page=2").then().body("data.first_name", hasItems("Lindsay","Ferguson"));     
		
	}
}
