package AssignmentRestAPI;


import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.Validatable;
import io.restassured.response.ValidatableResponse;
public class GoRest {
	
	@Test(priority = 0)
	public void testStatusCode() {
		baseURI = "https://gorest.co.in/";
		given().get("public/v2/users").then().statusCode(200);
	}	
	
	@Test(priority = 1)
	public void firstGetMethod() {
		Response res = RestAssured.get("https://gorest.co.in/public/v2/users");
		int statusCode = res.getStatusCode();
		System.out.println(statusCode);
		System.out.println(res.getBody().asString());
		
	}	
	
	@Test(priority = 2)
	private void assertGetMethod() {		
		Response res = RestAssured.get("https://gorest.co.in/public/v2/users");
		int statusCode = res.getStatusCode();		
		Assert.assertEquals(200, statusCode);	
		}
	
	@Test(priority = 3)
	public void postOperationTest() {
		JSONObject req = new JSONObject();
		req.put("name", "mohit");
		req.put("email", "mohits1mail.com");
		req.put("gender", "male");
		req.put("status", "Active");
		System.out.println(req);
		baseURI = "https://gorest.co.in/public/v2";
	ValidatableResponse res = given().log().all().contentType("application/json")
				.header("authorization", "Bearer 2a1b912708dc70387080d7256a82521fc550ee0ebbcaf42ba64e480f67a90a90")
				.body(req.toJSONString()).when().post("/users").then().statusCode(201);
			}	@Test(priority = 4)
	public void putOperationTest() {
		JSONObject req = new JSONObject();
		req.put("name", "Mohit");
		req.put("email", "mohits@gmmail.com.com");
		req.put("gender", "male");
		req.put("status", "Active");
		System.out.println(req);
		baseURI = "https://gorest.co.in/public/v2";
		given().log().all().contentType("application/json")
				.header("authorization", "Bearer 2a1b912708dc70387080d7256a82521fc550ee0ebbcaf42ba64e480f67a90a90")
				.body(req.toJSONString()).when().put("/users/192912").then()
				.statusCode(200);	}	@Test(priority = 5)
	public void DeleteOperationTest() {
		baseURI = "https://gorest.co.in/public/v2";
		given().log().all().contentType("application/json")
				.header("authorization", "Bearer 2a1b912708dc70387080d7256a82521fc550ee0ebbcaf42ba64e480f67a90a90")
				.when().delete("/users/192912").then().statusCode(204);	}

}
